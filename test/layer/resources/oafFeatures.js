export const featureCollection = {
    "type": "FeatureCollection",
    "features": [
        {
            "type": "Feature",
            "id": "DE.HH.UP_STAATLICHE_SCHULEN_441696",
            "geometry": {
                "type": "Point",
                "coordinates": [
                    564532,
                    5933771
                ]
            },
            "properties": {
                "ansprechp_buero": "Karin Stelling",
                "stadtteil": "Neustadt",
                "schul_homepage": "http://www.fsp2.hamburg.de",
                "is_rebbz": "false",
                "schul_email": "BS21@hibb.hamburg.de",
                "schulportrait": "http://hibb.hamburg.de/schulen/schulportraits/bs21",
                "sozialindex": "n.v.",
                "schulaufsicht": "Susanne Kallies",
                "schulform": "Berufsfachschule|Berufsschule|Berufsvorbereitungsschule|Fachschule",
                "fax": "+49 40 428 11-3339",
                "rechtsform": "staatlich",
                "schulname": "Staatliche Fachschule für Sozialpädagogik Altona",
                "lgv_standortk_erwachsenenbildung": "No",
                "adresse_strasse_hausnr": "Zeughausmarkt 32",
                "kernzeitbetreuung": "",
                "anzahl_schueler_gesamt": "1674 an 3 Standorten",
                "schul_id": "5964-3",
                "adresse_ort": "20459 Hamburg",
                "schul_telefonnr": "+49 40 428 11-3313",
                "abschluss": "Abschlusszeugnis|Abschlusszeugnis mit dem schulischen Teil der FHR|Abschlusszeugnis mit erweitertem erstem allgemeinbildenden Schulabschluss|Abschlusszeugnis mit Fachhochschulreife|Abschlusszeugnis mit mittleren Schulabschluss",
                "bezirk": "Hamburg-Mitte",
                "name_schulleiter": "Cornelia Averhoff",
                "schultyp": "Zweigstelle",
                "standort_id": "1078",
                "name_stellv_schulleiter": "Lennart Tienken",
                "kapitelbezeichnung": "Berufliche Schulen"
            },
            "srsName": "EPSG:25832"
        }
    ],
    "links": [
        {
            "href": "https://api.hamburg.de/datasets/v1/schulen/collections/staatliche_schulen/items?offset=440&crs=EPSG%3A25832&limit=10",
            "rel": "self",
            "type": "application/geo+json",
            "title": "this document as JSON"
        },
        {
            "href": "https://api.hamburg.de/datasets/v1/schulen/collections/staatliche_schulen/items?offset=440&crs=EPSG%3A25832&limit=10",
            "rel": "alternate",
            "type": "text/html",
            "title": "this document as HTML"
        },
        {
            "href": "https://api.hamburg.de/datasets/v1/schulen/collections/staatliche_schulen/items?offset=440&crs=EPSG%3A25832&limit=10",
            "rel": "alternate",
            "type": "application/gml+xml",
            "title": "this document as GML"
        },
        {
            "href": "https://api.hamburg.de/datasets/v1/schulen/collections/staatliche_schulen/items?offset=440&crs=EPSG%3A25832&limit=10",
            "rel": "alternate",
            "type": "application/gml+xml;version=3.2",
            "title": "this document as GML"
        },
        {
            "href": "https://api.hamburg.de/datasets/v1/schulen/collections/staatliche_schulen/items?offset=440&crs=EPSG%3A25832&limit=10",
            "rel": "alternate",
            "type": "application/gml+xml;version=3.2;profile=\"http://www.opengis.net/def/profile/ogc/2.0/gml-sf0\"",
            "title": "this document as GML"
        },
        {
            "href": "https://api.hamburg.de/datasets/v1/schulen/collections/staatliche_schulen/items?offset=440&crs=EPSG%3A25832&limit=10",
            "rel": "alternate",
            "type": "application/gml+xml;version=3.2;profile=\"http://www.opengis.net/def/profile/ogc/2.0/gml-sf2\"",
            "title": "this document as GML"
        },
        {
            "href": "https://api.hamburg.de/datasets/v1/schulen/collections/staatliche_schulen",
            "rel": "collection",
            "type": "application/json",
            "title": "Collection as JSON"
        },
        {
            "href": "https://api.hamburg.de/datasets/v1/schulen/collections/staatliche_schulen",
            "rel": "collection",
            "type": "text/html",
            "title": "Collection as HTML"
        },
        {
            "href": "https://api.hamburg.de/datasets/v1/schulen/collections/staatliche_schulen",
            "rel": "collection",
            "type": "application/xml",
            "title": "Collection as XML"
        }
    ],
    "numberMatched": 1,
    "numberReturned": 1,
    "timeStamp": "2022-03-29T13:39:55.511Z",
    "crs": "EPSG:25832"
};
